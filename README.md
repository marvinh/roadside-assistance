# Roadside assistance
This is a demo project for my application at Navimatrix. 

# Scenario:
A roadside assistance shall be provided to the customers. The client will provide an app to the customers which they can
use to report a breakdown. When the customer has opened a case, the app sends a data packet containing the personal data
of the customer, the language, data about the vehicle and diagnostic data about the problem via REST or SMS as a fallback
when the smartphone currently has no access to the internet. After the data packet has been sent the app starts a call 
which is being answered by a call center agent. For the processing of the case, the agent shall have access to the data
which have been sent previously. With the received data the agent can call the towing service and book a hotel or a rental
car for the customer. The client wants to be able to view the submitted cases and the status of their processing.

# Requirements:
- The system has to have a high availability (more than 99.9 percent)
- The system has to be scalable
- The solution is being used worldwide and supports multiple languages. The customer will talk in his birth language to
  the call center agent

# Task 1: conception
- The scenario shall be described visually
- A concept shall be created that describes the possible architecture of a system that implements the scenario
- The components to be used shall be described
- The biggest challenges that may occur during implementation shall be described

# Task 2: implementation
A software shall be implemented that collects the data of a case and merges it with the ingoing phone call from the app. 
The SMS contains data like the language of the caller and the type of the call trigger (automatic or manually).
For the processing of the callers issue a ticket shall be created. That ticket is active until it is closed by the
call center agent. Every call will be linked to that ticket until it is closed. Later calls create a new ticket.
For protecting the call center agents from many requests from callers, the amount of allowed calls per user within a
defined time span shall be limited. Calls that exceed that limit shall be blocked an delegated to a voice prompt. Every
try of a call shall extend the block time. Furthermore there shall be the option to block or allow specific callers
permanently. In the case that an SMS with automaticActivation=true is received, an exception shall be added that calls
to this number are not blocked for x minutes.