package cep.implementation;

import cep.Backend;
import cep.api.backend.Call;
import cep.api.backend.Data;
import cep.api.storage.record.DataRecord;
import cep.api.storage.record.FirewallRecord;
import cep.api.storage.record.TicketRecord;
import cep.api.storage.repository.DataRepository;
import cep.api.storage.repository.FirewallRepository;
import cep.api.storage.repository.TicketRepository;
import com.google.gson.Gson;
import liquibase.pro.packaged.D;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.MOCK,
        classes = Backend.class)
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class RESTControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private DataRepository dataRepository;

    @Autowired
    private FirewallRepository firewallRepository;

    @Autowired
    private TicketRepository ticketRepository;

    @Test
    public void incomingData() throws Exception {
        Data data = Data.builder()
                .phone("012345")
                .automaticActivation(true)
                .language("de")
                .build();

        assertEquals(0, StreamSupport.stream(ticketRepository.findAll().spliterator(), false).count());

        mvc.perform(put("/data")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new Gson().toJson(data))
        ).andExpect(status().isOk());

        assertEquals(1, StreamSupport.stream(ticketRepository.findAll().spliterator(), false).count());

        Data data2 = Data.builder()
                .phone("012345")
                .automaticActivation(false)
                .language("de")
                .build();

        mvc.perform(put("/data")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new Gson().toJson(data))
        ).andExpect(status().isOk());

        assertEquals(1, StreamSupport.stream(ticketRepository.findAll().spliterator(), false).count());

        assertTrue(StreamSupport.stream(ticketRepository.findAll().spliterator(), false)
                .anyMatch(ticketRecord ->
                        ticketRecord.getDataRecord().getPhone().equalsIgnoreCase("012345")
                                && !ticketRecord.getTicketId().isEmpty()
                                && ticketRecord.getDataRecord().isAutomaticActivation()));
    }

    @Test
    public void closeTicket() throws Exception {
        DataRecord dataRecord = new DataRecord();
        dataRecord.setPhone("012341");
        dataRecord.setLanguage("de");
        dataRecord.setAutomaticActivation(false);
        dataRepository.save(dataRecord);

        TicketRecord ticket = new TicketRecord();
        ticket.setDataRecord(dataRecord);
        ticket = ticketRepository.save(ticket);

        assertNotNull(ticket.getTicketId());
        assertFalse(ticket.getTicketId().isEmpty());

        mvc.perform(patch(String.format("/ticket/%s/close", ticket.getTicketId())))
                .andExpect(status().isOk());

        Optional<TicketRecord> storedTicket = ticketRepository.findById(ticket.getTicketId());
        assertTrue(storedTicket.isPresent());
        assertTrue(storedTicket.get().isClosed());
    }

    @Test
    public void setPhoneAllowed() throws Exception {
       assertFalse(firewallRepository.existsById("012347"));

        mvc.perform(patch(String.format("/firewall/%s/allow", "012347")))
                .andExpect(status().isOk());

        assertTrue(firewallRepository.existsById("012347"));
        assertTrue(firewallRepository.findById("012347").get().isPermanentAllowed());
        assertFalse(firewallRepository.findById("012347").get().isPermanentBlocked());
    }

    @Test
    public void setPhoneBlocked() throws Exception {
        assertFalse(firewallRepository.existsById("012346"));

        mvc.perform(patch(String.format("/firewall/%s/block", "012346")))
                .andExpect(status().isOk());

        assertTrue(firewallRepository.existsById("012346"));
        assertTrue(firewallRepository.findById("012346").get().isPermanentBlocked());
        assertFalse(firewallRepository.findById("012346").get().isPermanentAllowed());
    }

    @Test
    public void resetFirewallEntry() throws Exception {
        assertFalse(firewallRepository.existsById("012312"));

        FirewallRecord firewallRecord = new FirewallRecord();
        firewallRecord.setPhone("012312");
        firewallRecord.setPermanentBlocked(true);
        firewallRepository.save(firewallRecord);

        mvc.perform(patch(String.format("/firewall/%s/reset", "012312")))
                .andExpect(status().isOk());
        firewallRecord = firewallRepository.findById("012312").get();
        assertFalse(firewallRecord.isPermanentAllowed());
        assertFalse(firewallRecord.isPermanentBlocked());
        assertNull(firewallRecord.getTemporaryBlockDeadline());
        assertNull(firewallRecord.getTemporaryAllowanceDeadline());
    }

    @Test
    public void callHandlingNormal() throws Exception {
        Data data = Data.builder()
                .language("de")
                .phone("012348")
                .automaticActivation(false)
                .build();



        mvc.perform(put("/data")
                .content(new Gson().toJson(data))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        LocalDateTime windowStart = LocalDateTime.now();
        LocalDateTime windowEnd = windowStart.plus(Duration.ofMinutes(1));

        long sequenceNumber = 0;
        while(LocalDateTime.now().compareTo(windowEnd) <= 0){

            Call call = Call.builder()
                    .interactionId(sequenceNumber)
                    .phone("012348")
                    .build();

            String result = mvc.perform(post("/call")
                            .content(new Gson().toJson(call))
                            .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andReturn()
                    .getResponse().getContentAsString();
            @SuppressWarnings("unchecked") Map<String, Object> callResponse = new Gson().fromJson(result, Map.class);
            if (sequenceNumber < 3){
                assertFalse((Boolean) callResponse.get("blocked"));
            }
            else {
                assertTrue((Boolean) callResponse.get("blocked"));
            }

            assertFalse((Boolean) callResponse.get("automaticActivation"));
            assertEquals("de", callResponse.get("language"));
            assertNotNull(callResponse.get("ticketId"));
            assertFalse(((String)callResponse.get("ticketId")).isEmpty());

            sequenceNumber++;
        }

        Thread.sleep(60001); //sleep a little bit more than one minute to make the ban to timeout
        assert(sequenceNumber >= 3); //at this point, at least 4 calls should have been sent to the backend

        Call call = Call.builder()
                .interactionId(sequenceNumber)
                .phone("012348")
                .build();
        String result = mvc.perform(post("/call")
                        .content(new Gson().toJson(call))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse().getContentAsString();
        @SuppressWarnings("unchecked") Map<String, Object> callResponse = new Gson().fromJson(result, Map.class);
        assertFalse((Boolean) callResponse.get("blocked"));
        assertFalse((Boolean) callResponse.get("automaticActivation"));
        assertEquals("de", callResponse.get("language"));
        assertNotNull(callResponse.get("ticketId"));
        assertFalse(((String)callResponse.get("ticketId")).isEmpty());
    }

    @Test
    public void callHandlingPermaBan() throws Exception {
        assertFalse(firewallRepository.existsById("012349"));

        mvc.perform(patch(String.format("/firewall/%s/block", "012349")))
                .andExpect(status().isOk());

        assertTrue(firewallRepository.existsById("012349"));
        assertTrue(firewallRepository.findById("012349").get().isPermanentBlocked());
        assertFalse(firewallRepository.findById("012349").get().isPermanentAllowed());

        Data data = Data.builder()
                .language("de")
                .phone("012349")
                .automaticActivation(false)
                .build();



        mvc.perform(put("/data")
                        .content(new Gson().toJson(data))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        LocalDateTime windowStart = LocalDateTime.now();
        LocalDateTime windowEnd = windowStart.plus(Duration.ofMinutes(1));

        long sequenceNumber = 0;
        while(LocalDateTime.now().compareTo(windowEnd) <= 0){

            Call call = Call.builder()
                    .interactionId(sequenceNumber)
                    .phone("012349")
                    .build();

            String result = mvc.perform(post("/call")
                            .content(new Gson().toJson(call))
                            .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andReturn()
                    .getResponse().getContentAsString();
            @SuppressWarnings("unchecked") Map<String, Object> callResponse = new Gson().fromJson(result, Map.class);
            assertTrue((Boolean) callResponse.get("blocked"));
            assertFalse((Boolean) callResponse.get("automaticActivation"));
            assertEquals("de", callResponse.get("language"));
            assertNotNull(callResponse.get("ticketId"));
            assertFalse(((String)callResponse.get("ticketId")).isEmpty());

            sequenceNumber++;
        }

        Thread.sleep(60001); //sleep a little bit more than one minute to ensure the ban timed out if it was a temporary ban
        assert(sequenceNumber >= 3); //at this point, at least 4 calls should have been sent to the backend

        Call call = Call.builder()
                .interactionId(sequenceNumber)
                .phone("012349")
                .build();
        String result = mvc.perform(post("/call")
                        .content(new Gson().toJson(call))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse().getContentAsString();
        @SuppressWarnings("unchecked") Map<String, Object> callResponse = new Gson().fromJson(result, Map.class);
        assertTrue((Boolean) callResponse.get("blocked"));
        assertFalse((Boolean) callResponse.get("automaticActivation"));
        assertEquals("de", callResponse.get("language"));
        assertNotNull(callResponse.get("ticketId"));
        assertFalse(((String)callResponse.get("ticketId")).isEmpty());
    }

    @Test
    public void callHandlingPermaAllowance() throws Exception {
        assertFalse(firewallRepository.existsById("012310"));

        mvc.perform(patch(String.format("/firewall/%s/allow", "012310")))
                .andExpect(status().isOk());

        assertTrue(firewallRepository.existsById("012310"));
        assertTrue(firewallRepository.findById("012310").get().isPermanentAllowed());
        assertFalse(firewallRepository.findById("012310").get().isPermanentBlocked());

        Data data = Data.builder()
                .language("de")
                .phone("012310")
                .automaticActivation(false)
                .build();



        mvc.perform(put("/data")
                        .content(new Gson().toJson(data))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        LocalDateTime windowStart = LocalDateTime.now();
        LocalDateTime windowEnd = windowStart.plus(Duration.ofMinutes(1));

        long sequenceNumber = 0;
        while(LocalDateTime.now().compareTo(windowEnd) <= 0){

            Call call = Call.builder()
                    .interactionId(sequenceNumber)
                    .phone("012310")
                    .build();

            String result = mvc.perform(post("/call")
                            .content(new Gson().toJson(call))
                            .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andReturn()
                    .getResponse().getContentAsString();
            @SuppressWarnings("unchecked") Map<String, Object> callResponse = new Gson().fromJson(result, Map.class);
            assertFalse((Boolean) callResponse.get("blocked"));
            assertFalse((Boolean) callResponse.get("automaticActivation"));
            assertEquals("de", callResponse.get("language"));
            assertNotNull(callResponse.get("ticketId"));
            assertFalse(((String)callResponse.get("ticketId")).isEmpty());

            sequenceNumber++;
        }

        Thread.sleep(1); //sleep a little bit more than one minute to ensure the allowance has timed out if it was temporary
        assert(sequenceNumber >= 3); //at this point, at least 4 calls should have been sent to the backend

        Call call = Call.builder()
                .interactionId(sequenceNumber)
                .phone("012310")
                .build();
        String result = mvc.perform(post("/call")
                        .content(new Gson().toJson(call))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse().getContentAsString();
        @SuppressWarnings("unchecked") Map<String, Object> callResponse = new Gson().fromJson(result, Map.class);
        assertFalse((Boolean) callResponse.get("blocked"));
        assertFalse((Boolean) callResponse.get("automaticActivation"));
        assertEquals("de", callResponse.get("language"));
        assertNotNull(callResponse.get("ticketId"));
        assertFalse(((String)callResponse.get("ticketId")).isEmpty());
    }

    @Test
    public void callHandlingTempAllowance() throws Exception {
        Data data = Data.builder()
                .language("de")
                .phone("012311")
                .automaticActivation(true)
                .build();

        LocalDateTime windowStart = LocalDateTime.now();
        LocalDateTime windowEnd = windowStart.plus(Duration.ofMinutes(1));

        mvc.perform(put("/data")
                        .content(new Gson().toJson(data))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        long sequenceNumber = 0;
        while(LocalDateTime.now().compareTo(windowEnd) <= 0){

            Call call = Call.builder()
                    .interactionId(sequenceNumber)
                    .phone("012311")
                    .build();

            String result = mvc.perform(post("/call")
                            .content(new Gson().toJson(call))
                            .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andReturn()
                    .getResponse().getContentAsString();
            @SuppressWarnings("unchecked") Map<String, Object> callResponse = new Gson().fromJson(result, Map.class);
            assertFalse((Boolean) callResponse.get("blocked"));
            assertTrue((Boolean) callResponse.get("automaticActivation"));
            assertEquals("de", callResponse.get("language"));
            assertNotNull(callResponse.get("ticketId"));
            assertFalse(((String)callResponse.get("ticketId")).isEmpty());

            sequenceNumber++;
        }

        Thread.sleep(3000); //sleep a little bit more than one minute to make the temporary allowance to timeout
        assert(sequenceNumber >= 3); //at this point, at least 4 calls should have been sent to the backend

        Call call = Call.builder()
                .interactionId(sequenceNumber)
                .phone("012311")
                .build();
        String result = mvc.perform(post("/call")
                        .content(new Gson().toJson(call))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse().getContentAsString();
        @SuppressWarnings("unchecked") Map<String, Object> callResponse = new Gson().fromJson(result, Map.class);
        assertTrue((Boolean) callResponse.get("blocked"));
        assertFalse((Boolean) callResponse.get("automaticActivation"));
        assertEquals("de", callResponse.get("language"));
        assertNotNull(callResponse.get("ticketId"));
        assertFalse(((String)callResponse.get("ticketId")).isEmpty());
    }
}