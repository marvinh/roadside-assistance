package cep.api.backend;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

@Builder
@Data
@NonNull
public class Call {
    private final String phone;
    private final long interactionId;
}
