package cep.api.backend;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

import java.time.LocalDateTime;

@Builder
@Data
@NonNull
public class CallResponse {
    private final LocalDateTime ticketCreationTimestamp;
    private final String ticketId;
    private final String language;
    private final boolean automaticActivation;
    private final boolean blocked;
}
