package cep.api.backend;

import lombok.Builder;
import lombok.NonNull;

@Builder
@lombok.Data
@NonNull
public class Data {
    private final String phone;
    private final String language;
    private final boolean automaticActivation;
}
