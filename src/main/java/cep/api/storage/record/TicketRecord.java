package cep.api.storage.record;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Entity
@Getter
@Setter
public class TicketRecord {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String ticketId;

    private LocalDateTime creationDate = LocalDateTime.now();

    @ManyToOne(optional = false)
    private DataRecord dataRecord;

    @ManyToMany
    private final List<CallRecord> calls = new LinkedList<>();

    private boolean closed;
}
