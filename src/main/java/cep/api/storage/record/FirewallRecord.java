package cep.api.storage.record;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class FirewallRecord {
    @Id
    private String phone;

    private boolean permanentBlocked;
    private boolean permanentAllowed;
    private LocalDateTime temporaryAllowanceDeadline;
    private LocalDateTime temporaryBlockDeadline;
}
