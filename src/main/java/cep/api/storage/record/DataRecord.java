package cep.api.storage.record;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Getter
@Setter
public class DataRecord {
    @Id
    @GeneratedValue
    private Long id;

    private String phone;
    private String language;
    private boolean automaticActivation;
}
