package cep.api.storage.record;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class CallRecord {
    @Id
    private Long interactionId;

    private LocalDateTime date;

    private String phone;

    private boolean blocked;
}
