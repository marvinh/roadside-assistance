package cep.api.storage.repository;

import cep.api.storage.record.DataRecord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories
public interface DataRepository extends JpaRepository<DataRecord, Long> {
}
