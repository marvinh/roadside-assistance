package cep.api.storage.repository;

import cep.api.storage.record.CallRecord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories
public interface CallRepository extends JpaRepository<CallRecord, Long> {
}
