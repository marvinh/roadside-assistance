package cep.api.storage.repository;

import cep.api.storage.record.TicketRecord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.util.Optional;

@EnableJpaRepositories
public interface TicketRepository extends JpaRepository<TicketRecord, String> {
    @Query("SELECT u FROM TicketRecord u WHERE u.dataRecord.phone = ?1 and u.closed = false ORDER BY u.creationDate DESC")
    Optional<TicketRecord> findMostRecentOpenTicketByPhoneNumber(String phone);
}
