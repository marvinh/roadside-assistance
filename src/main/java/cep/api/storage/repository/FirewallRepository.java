package cep.api.storage.repository;

import cep.api.storage.record.FirewallRecord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories
public interface FirewallRepository extends JpaRepository<FirewallRecord, String> {
}
