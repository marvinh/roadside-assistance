package cep.implementation.mapper;

import cep.api.backend.Data;
import cep.api.storage.record.DataRecord;
import org.mapstruct.Mapper;

@Mapper
public interface DataRecordMapper {
    DataRecord toRecord(Data data);
    Data fromRecord(DataRecord dataRecord);
}
