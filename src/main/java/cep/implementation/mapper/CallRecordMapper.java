package cep.implementation.mapper;

import cep.api.backend.Call;
import cep.api.storage.record.CallRecord;
import org.mapstruct.Mapper;

@Mapper
public interface CallRecordMapper {
    CallRecord toRecord(Call call);
    Call fromRecord(CallRecord callRecord);
}
