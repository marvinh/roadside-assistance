package cep.implementation;

import cep.api.backend.Call;
import cep.api.backend.CallResponse;
import cep.api.backend.Data;
import cep.api.storage.record.CallRecord;
import cep.api.storage.record.DataRecord;
import cep.api.storage.record.FirewallRecord;
import cep.api.storage.record.TicketRecord;
import cep.api.storage.repository.CallRepository;
import cep.api.storage.repository.DataRepository;
import cep.api.storage.repository.FirewallRepository;
import cep.api.storage.repository.TicketRepository;
import cep.implementation.mapper.CallRecordMapper;
import cep.implementation.mapper.DataRecordMapper;
import lombok.RequiredArgsConstructor;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Optional;

@Controller
@RequiredArgsConstructor
public class RESTController {
    private final DataRepository dataRepository;
    private final TicketRepository ticketRepository;

    private final FirewallRepository firewallRepository;

    private final CallRepository callRepository;

    @Qualifier("temporary-block-duration")
    private final Duration temporaryBlockDuration;

    @Qualifier("temporary-allowance-duration")
    private final Duration temporaryAllowanceDuration;

    @Qualifier("calls-per-period")
    private final Long callsPerPeriod;

    @Qualifier("call-limit-period")
    private final Duration callLimitPeriod;

    private final DataRecordMapper dataRecordMapper = Mappers.getMapper(DataRecordMapper.class);

    private final CallRecordMapper callRecordMapper = Mappers.getMapper(CallRecordMapper.class);

    private TicketRecord findOpenTicket(String phone){
        Optional<TicketRecord> ticket = ticketRepository.findMostRecentOpenTicketByPhoneNumber(phone);

        return ticket.orElse(null);
    }

    @PutMapping("/data")
    public ResponseEntity<Void> incomingData(@RequestBody Data data) {
        TicketRecord ticket = findOpenTicket(data.getPhone());

        if (ticket == null){
            DataRecord dataRecord = dataRepository.saveAndFlush(dataRecordMapper.toRecord(data));
            TicketRecord ticketRecord = new TicketRecord();
            ticketRecord.setDataRecord(dataRecord);
            ticketRepository.saveAndFlush(ticketRecord);

            if (data.isAutomaticActivation()){
                FirewallRecord firewallRecord = new FirewallRecord();
                firewallRecord.setPhone(data.getPhone());
                firewallRecord.setTemporaryAllowanceDeadline(LocalDateTime.now().plus(temporaryAllowanceDuration));
                firewallRepository.saveAndFlush(firewallRecord);
            }
        }

        return ResponseEntity.ok().build();
    }

    @PatchMapping("/ticket/{id}/close")
    public ResponseEntity<Void> closeTicket(@PathVariable String id){
        Optional<TicketRecord> ticket = ticketRepository.findById(id);
        if (ticket.isEmpty()){
            return ResponseEntity.notFound().build();
        }

        ticket.get().setClosed(true);
        ticketRepository.saveAndFlush(ticket.get());
        return ResponseEntity.ok().build();
    }

    @PatchMapping("/firewall/{phone}/allow")
    public ResponseEntity<Void> allowPhone(@PathVariable String phone){
        FirewallRecord firewallRecord = getFirewallRecord(phone);

        firewallRecord.setPermanentAllowed(true);
        firewallRecord.setPermanentBlocked(false);
        firewallRecord.setTemporaryAllowanceDeadline(null);
        firewallRecord.setTemporaryBlockDeadline(null);

        firewallRepository.saveAndFlush(firewallRecord);
        return ResponseEntity.ok().build();
    }

    @PatchMapping("/firewall/{phone}/block")
    public ResponseEntity<Void> blockPhone(@PathVariable String phone){
        FirewallRecord firewallRecord = getFirewallRecord(phone);

        firewallRecord.setPermanentAllowed(false);
        firewallRecord.setPermanentBlocked(true);
        firewallRecord.setTemporaryAllowanceDeadline(null);
        firewallRecord.setTemporaryBlockDeadline(null);

        firewallRepository.saveAndFlush(firewallRecord);
        return ResponseEntity.ok().build();
    }

    @PatchMapping("/firewall/{phone}/reset")
    public ResponseEntity<Void> resetFirewallEntry(@PathVariable String phone){
        FirewallRecord firewallRecord = getFirewallRecord(phone);

        firewallRecord.setPermanentAllowed(false);
        firewallRecord.setPermanentBlocked(false);
        firewallRecord.setTemporaryAllowanceDeadline(null);
        firewallRecord.setTemporaryBlockDeadline(null);

        firewallRepository.saveAndFlush(firewallRecord);
        return ResponseEntity.ok().build();
    }

    private FirewallRecord getFirewallRecord(String phone){
        if (!firewallRepository.existsById(phone)){
            FirewallRecord firewallRecord = new FirewallRecord();
            firewallRecord.setPhone(phone);
            firewallRepository.saveAndFlush(firewallRecord);
        }


        Optional<FirewallRecord> firewallRecord = firewallRepository.findById(phone);
        assert firewallRecord.isPresent();
        return firewallRecord.get();
    }

    @PostMapping("/call")
    public ResponseEntity<CallResponse> incomingCall(@RequestBody Call call){
        TicketRecord ticket = findOpenTicket(call.getPhone());

        if (ticket == null){
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
        }

        CallRecord callRecord = callRecordMapper.toRecord(call);
        callRecord.setDate(LocalDateTime.now());
        callRepository.saveAndFlush(callRecord);
        ticket.getCalls().add(callRecord);
        ticketRepository.saveAndFlush(ticket);

        FirewallRecord firewallRecord = applyTempRules(call, ticket);
        firewallRepository.saveAndFlush(firewallRecord);

        return ResponseEntity.ok(CallResponse.builder()
                        .ticketId(ticket.getTicketId())
                        .ticketCreationTimestamp(ticket.getCreationDate())
                        .language(ticket.getDataRecord().getLanguage())
                        .blocked(isBlocked(firewallRecord))
                        .automaticActivation(isAutomaticActivation(firewallRecord))
                .build());
    }

    private FirewallRecord applyTempRules(Call call, TicketRecord ticketRecord){
        FirewallRecord firewallRecord = new FirewallRecord();
        firewallRecord.setPhone(call.getPhone());

        if (firewallRepository.existsById(call.getPhone())){
            Optional<FirewallRecord> foundFirewallRecord = firewallRepository.findById(call.getPhone());
            assert foundFirewallRecord.isPresent();
            firewallRecord = foundFirewallRecord.get();
        }

        if (firewallRecord.isPermanentAllowed() || firewallRecord.isPermanentBlocked()){
            return firewallRecord;
        }

        if (isAutomaticActivation(firewallRecord)){
            return firewallRecord;
        }

        if (firewallRecord.getTemporaryBlockDeadline() != null){
            if (LocalDateTime.now().compareTo(firewallRecord.getTemporaryBlockDeadline()) <= 0){
                firewallRecord.setTemporaryBlockDeadline(LocalDateTime.now().plus(temporaryBlockDuration));
                return firewallRecord;
            }
        }

        if (
                ticketRecord.getCalls().stream()
                        .filter(callRecord -> callRecord.getDate().compareTo(LocalDateTime.now().minus(callLimitPeriod)) >= 0)
                        .filter(callRecord -> callRecord.getDate().compareTo(LocalDateTime.now()) <= 0)
                        .count() > callsPerPeriod
        ){
            firewallRecord.setTemporaryBlockDeadline(LocalDateTime.now().plus(temporaryBlockDuration));
        }

        return firewallRecord;
    }

    private boolean isAutomaticActivation(FirewallRecord firewallRecord){
        if (firewallRecord.getTemporaryAllowanceDeadline() != null){
            if (LocalDateTime.now().compareTo(firewallRecord.getTemporaryAllowanceDeadline()) <= 0){
                return true;
            }
        }
        return false;
    }

    private boolean isBlocked(FirewallRecord firewallRecord){
        boolean permanentlyBlocked = firewallRecord.isPermanentBlocked();

        boolean temporaryBlocked = false;
        if (firewallRecord.getTemporaryBlockDeadline() != null){
            if (LocalDateTime.now().compareTo(firewallRecord.getTemporaryBlockDeadline()) <= 0){
                temporaryBlocked = true;
            }
        }

        return permanentlyBlocked || temporaryBlocked;
    }
}
