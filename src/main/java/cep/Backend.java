package cep;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(considerNestedRepositories = true)
public class Backend implements ApplicationRunner {

    public static void main(String[] args) {
        SpringApplication.run(Backend.class, args);
    }

    @Override
    public void run(ApplicationArguments args) {

    }
}
