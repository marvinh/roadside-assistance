package cep;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.Duration;

@Configuration
public class BackendConfiguration {
    @Value("${cep.backend.temp.allow:PT1H30M}")
    private String temporaryAllowanceDurationString;

    @Value("${cep.backend.temp.ban:PT1H30M}")
    private String temporaryBlockDurationString;

    @Value("${cep.backend.limit.calls.amount:5}")
    private String callsPerPeriodString;

    @Value("${cep.backend.limit.calls.period:PT5M}")
    private String callLimitPeriodString;

    @Bean(name = "temporary-allowance-duration")
    public Duration temporaryAllowanceDuration(){
        assert temporaryAllowanceDurationString != null;
        return Duration.parse(temporaryAllowanceDurationString);
    }

    @Bean(name = "temporary-block-duration")
    public Duration temporaryBlockDuration(){
        assert temporaryBlockDurationString != null;
        return Duration.parse(temporaryBlockDurationString);
    }

    @Bean(name = "calls-per-period")
    public Long callsPerPeriod(){
        assert callsPerPeriodString != null;
        return Long.parseLong(callsPerPeriodString);
    }

    @Bean(name = "call-limit-period")
    public Duration callLimitPeriod(){
        assert callLimitPeriodString != null;
        return Duration.parse(callLimitPeriodString);
    }
}
