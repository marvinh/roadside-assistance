# API documentation
In this document I will describe the API that I have implemented for part 2
of the trial task.

## Object types
### Data
- phone: String
- language: String
- automaticActivation: boolean

### Call
- phone: String
- interactionId: long

### CallResponse
- ticketCreationTimestamp: LocalDateTime
- ticketId: String
- language: String
- automaticActivation: boolean
- blocked: boolean

## REST Endpoints
### /data
Method: Put
Request content: Data object

This endpoint stores the transmitted data object into the database and creates
a new ticket associated with it. If an open ticket with the phone number given in the
data object already exists, no new ticket will be created.
If ```automaticActivation``` in the Data object is set to true, A firewall allowance
is set so that incoming calls does not trigger a temporary ban when they exceed
the set limit.

### /ticket/{id}/close
Method: Patch
Path variable: Ticket id

This enpoint closes the ticket with the given ID.

### /firewall/{phone}/allow
Method: Patch
Path variable: Phone number

Adds a permanent allowance for the given phone number, so that it will not get
blocked when exceeding the limit of allowed phone calls per minute.

### /firewall/{phone}/block
Method: Patch
Path variable: Phone number

Adds a permanent block for the given phone number, so that all calls from this
phone number will get blocked, whether they exceed the rate limit or not.

### /firewall/{phone}/reset
Method: Patch
Path variable: Phone number

Removes any allowances or bans from the given phone number.

### /call
Method: Post
Request content: Call object
Response content: CallResponse object

Accepts a call and matches it with a related open ticket. If no ticket is present, this request will return
status code 409 (Conflict). If a ban is present, whether temporary due to exceeding the rate limit or permanently,
the ```blocked``` field of the CallResponse is set to true. If a temporary allowance is set in the firewall,
the ```automaticActivation``` is set to true.