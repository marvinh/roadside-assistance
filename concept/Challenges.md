# Challenges
There are some bigger challenges which might occur during the implementation of the system.

## Challenges on the frontend
The frontend must be configurable for multiple languages. All texts on the frontend need to be localized and so
hard coding them is no solution. Since it is not part of my task to implement the frontend I will not research
on how to achieve this goal.

## Challenges on the backend implementation
On the backend side the controller must be able to handle incoming VoIP calls, so there need to be
a controller with an endpoint for it. Since Spring Boot is to be used for simplifying the effort in implementation
and maintenance a library have to be found that supports VoIP communication.

The backend needs to be able to receive SMS containing the diagnostics data. To simplify the implementation, I assume 
that the SMS sent by the application is being converted to a REST API call using a separate Gateway which is not part of
this implementation.

Also, the backend should be loosely coupled to the frontend through a well-defined API to ease the use of 
load balancing solutions to meet the high availability criteria.