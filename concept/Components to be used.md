# Components to be used
There are some technical components which I would use to implement the system.

## Components for the frontend
At the frontend - the part the user directly interacts with - I would use
a framework that allows for building Android and iOS apps from the same source
code to reduce the effort in maintaining the code base. A good framework for this
could be PhoneGap, which allows for writing the code in HTML5, CSS and Javascript
and compiles it into Android and iOS apps. Since the app shall be able to make
phone calls, I would add the NPM dependency "call-number". Also I would include
the npm dependency "cordova-sms-plugin" to allow the app to send SMS.

## Components for the backend
For the backend - the part that processes incoming requests from the frontend - I
would use Spring Boot for the implementation because it reduces the effort in maintenance
and implementation and allows for a lesser coupling of the source code. For storing data
permanently I will use JPA for this since I don't want to write a lot
of database queries on my own and instead let the framework do the work for me.